<?php
/* Call this file 'hello-world.php' */
require __DIR__ . '/vendor/mike42/escpos-php/autoload.php';

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\CapabilityProfile;
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;

$connector = new WindowsPrintConnector("POS-80-Series");
$printer = new Printer($connector);

//call api here
$handle = curl_init();
 
$url = "http://parlour.prawasinepali.com/api/get-reciept?id=" . $_GET["id"];
//$url = "http://spa.local/api/get-reciept?id=" . $_GET["id"];
 
// Set the url
curl_setopt($handle, CURLOPT_URL, $url);
// Set the result output to be a string.
curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
 
$output = curl_exec($handle);
 
curl_close($handle);

 $data = json_decode($output);

  var_dump($data);
  var_dump($data->transaction->customer_name);


   
   /* Information for the receipt */
   $service_items = [];
      foreach($data->services as $key => $service){
         $service_items[$key] = new item($service->title,
         $service->quantity, 
         $service->price);
  
     }
     $product_items = [];
     foreach($data->products as $key => $product){
        $product_items[$key] = new item($product->title,
        $product->quantity, 
        $product->price);
 
    }
      
  
   $subtotal = new item('Subtotal', ' ',$data->transaction->sub_total );
   $discount = new item('Discount',' ',$data->transaction->net_discount );
   if($data->transaction->coupon_discount > 0){

     $coupon_discount = new item('Coupon Discount','',$data->transaction->coupon_discount );
   }


  //  $tax = new item('A local tax','', '1.30');
  

   $total = new item('Total', ' ',$data->transaction->total_bill);
   /* Date is kept the same for testing */
   date_default_timezone_set($data->timezone);

 
   $date = date('l jS \of F Y h:i:s A');
   /* Start the printer */
   
  //  $logo = EscposImage::load('escpos-php.png', false);
  //  $printer = new Printer($connector);
   
   /* Print top logo */
  //   $printer->setJustification(Printer::JUSTIFY_CENTER);
  // $printer->graphics($logo);
   
   /* Name of shop */

   $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
   $printer->setJustification(Printer::JUSTIFY_CENTER);
   $printer->text("SPA Pvt. Ltd.\n");
   $printer->text("Prawasi Nepali, MADRID\n");
   $printer->selectPrintMode();
   $printer->text("9856008281.\n");
   $printer->feed();
   
   /* Title of receipt */
   $printer->setJustification(Printer::JUSTIFY_CENTER);
   $printer->setEmphasis(true);
   $printer->text("ABBREVIATED TAX INVOICE\n");
   $printer->setEmphasis(false);
   
   /* Items */
   

   $printer->feed();
   $printer->setJustification(Printer::JUSTIFY_LEFT);
   $printer->setEmphasis(true);
   $printer->text(new item('Particular', 'Qty', 'Price'));
   $printer->text("================================================");
   $printer->setEmphasis(true);
   foreach ($service_items as $item) {
       $printer->text($item);
   }
   foreach ($product_items as $item) {
    $printer->text($item);
}
   $printer->setEmphasis(false);

   $printer->feed();
   $printer->setEmphasis(true);
   $printer->text($subtotal);
   $printer->text($discount);
   if($data->transaction->coupon_discount > 0){
   $printer->text($coupon_discount);
   }
  
   $printer->feed();
   
   /* Tax and total */
  //  $printer->text($tax);

   $printer->text($total);
   $printer->setEmphasis(false);
   
   /* Footer */
   $printer->feed(2);
   $printer->setJustification(Printer::JUSTIFY_CENTER);
   $printer->text("Thank you for visiting us\n");
   $printer->text("For trading hours, please visit example.com\n");
   $printer->feed(1);
   $printer->text($date . "\n");
   
   /* Cut the receipt and open the cash drawer */
    $printer->cut();
   $printer->pulse();
   
   $printer->close();
                           
  


class item
{
    private $name;
    private $qty;
    private $price;
    private $dollarSign;

    public function __construct($name = '', $qty = '', $price = '',  $dollarSign = false)
    {
      $this->name = $name;
      $this->qty = $qty;
      $this->price = $price;
        $this->dollarSign = $dollarSign;
    }

    public function __toString()
    {
      $leftCols = 30;
      $centerCols = 10;
      $rightCols = 10;
        if ($this->dollarSign) {
            $leftCols = $leftCols / 2  - $rightCols / 2;
        }
        $left = str_pad($this->name, $leftCols ) ;
        $center = str_pad($this->qty, $centerCols) ;

        $sign = ($this->dollarSign ? 'Rs.' : '');
        $right = str_pad($sign . $this->price, $rightCols);
        return "$left$center$right\n";
    }
  }


?>
 <!-- <script type="text/javascript">location.href = 'http://parlour.prawasinepali.com/en/admin/counter';</script>  -->